<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Requests\ApiLoginRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Key;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public $good = true;
    
    public function apiLogin(ApiLoginRequest $request){
        if($request->input('email') === null){
            $this->good = false;
            $error[] = "Email is missing";
        }
        
        if($request->input('password') === null){
            $this->good = false;
            $error[] = "Password is missing";
        }
        
        if($this->good == true){
            $user = User::where(["email" => $request->input('email')])->first();
            if($user->key !== null){
                return response()->json([
                    "msg" => "Failed",
                    "errors" => "You already are logged in",                   
                ]);
            }
            if($user !== null){
                if(Hash::check($request->input('password'), $user->password)){
                    $key = new Key();
                    $key->user_id = $user->id;
                    $key->key = $key->make();
                    $key->save();
                    return response()->json([
                        "msg" => "Login successful, AuthToken generated",
                        "AuthToken" => $key->key
                    ]);
                }else{
                    return response()->json([
                        "msg" => "Login failed",
                        "errors" => "Password and Email do not match",
                        
                    ]);
                }
                
            }else{
                return response()->json([
                    "msg" => "Login failed",
                    "errors" => "Email not found",
                    
                ]);
            }
        }else{
            return response()->json([
                "msg" => "Login failed",
                "errors" => $errors
            ]);
        }
        
       
    }
    
    public function apiLoginGet(){
        
            return response()->json([
                "msg" => "Use POST method to login"
            ]);
        
    }
    
    public function print(Request $request){
        if($request->input('authtoken') === null){
            $this->good = false;
            $error[] = "Authtoken is missing";
        }
        if($this->good == true){
            $key = Key::where(["key" => $request->input('authtoken')])->first();
            if($key !== null){
            $user = $key->user;
            return response()->json([
                "userdata" => $user
            ]);
            }else{
                return response()->json([
                    "msg" => "Failed",
                    "errors" => "Authtoken not found",
                    
                ]);
            }
        }else{
            return response()->json([
                "msg" => "Failed",
                "errors" => $error,
                
            ]);
        }

        
    }
    
    public function apiLogout(Request $request){
        if($request->input('authtoken') === null){
            $this->good = false;
            $error[] = "Authtoken is missing";
        }
        if($this->good == true){
            $key = Key::where(["key" => $request->input('authtoken')])->first();
            if($key !== null){
                $key->delete();
                return response()->json([
                    "msg" => "You are logged out"
                ]);
            }else{
                return response()->json([
                    "msg" => "Failed",
                    "errors" => "Authtoken not found",
                    
                ]);
            }
        }else{
            return response()->json([
                "msg" => "Failed",
                "errors" => $error,
                
            ]);
        }
        
        
    }
}
