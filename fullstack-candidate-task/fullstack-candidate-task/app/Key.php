<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Key extends Model
{
    public function make(){
        return Str::random(64);
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
