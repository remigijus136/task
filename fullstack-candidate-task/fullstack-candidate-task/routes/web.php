<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('api/login', 'Controller@apiLogin')->name('apiLogin');

Route::get('api/login', 'Controller@apiLoginGet')->name('apiLoginGet');

Route::get('api/print', 'Controller@print')->name('print');

Route::get('api/logout', 'Controller@apiLogout')->name('apiLogout');
