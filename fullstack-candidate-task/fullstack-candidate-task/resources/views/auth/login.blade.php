@extends('layouts.app')

@section('content')
<div class="container mid">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">Login Form</div>

                <div class="card-body">
                    <form class="form-group" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">           

                            <div class="col-md-12">
                                <input type="text" id="username" name="username" class="form-control" placeholder="Username">
								    <span class="form-control-feedback input-img">
        								<i class="fa fa-user"></i>
    								</span>
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input type="password" class="form-control" name="password" required placeholder="Password">
                              		<span class="form-control-feedback input-img">
        								<i class="fa fa-lock"></i>
    								</span>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         <button type="submit" class="btn btn-primary mybtn">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
